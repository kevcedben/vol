var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');

var client = new cassandra.Client({contactPoints: ['127.0.0.1'], keyspace: 'demo'});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/vol', function(req, res, next) {
  client.execute("SELECT lastname, age, city, email, firstname FROM users ", function (err, result) {
    if (!err){
      if ( result.rows.length > 0 ) {
        var user = result.rows[0];
        console.log("name = %s, age = %d", user.firstname, user.age);
      } else {
        console.log("No results");
      }
    }

    // Run next function in series
    callback(err, null);
  });

});
router.get('/vol/api/docs', function(req, res, next) {
  res.sendfile('doc.html');
});

module.exports = router;
